#include "graphics.h"
#include "circulo.h"
#include <stack>
#include <thread>
#include <ctime>
#include <random>

int movimientoY;

int nHx;
int nHy;
int movHongoY;

int nHEx;
int nHEy;
int movHelicY;

int nAVx;
int nAVy;
int movAvioY;

int movimientoBala;

int numeroHongoX() {
	srand(0);
	nHx = 1400 + rand() % (2000);
	return nHx;
}

int numeroHongoY() {
	srand(0);
	nHy = 1 + rand() % (200);
	return nHy;
}

int numeroHelicX() {
	srand(50);
	nHEx = 1400 + rand() % (2000);
	return nHEx;
}

int numeroHelicY() {
	srand(0);
	nHEy = 200 + rand() % (400);
	return nHEy;
}

int numeroAvX() {
	srand(100);
	nAVx = 1400 + rand() % (2000);
	return nAVx;
}

int numeroAvY() {
	srand(0);
	nAVy = 400 + rand() % (600);
	return nAVy;
}

void milinea(int x1, int y1, int x2, int y2, int color) {
	stack<Coordenadas>pila = linea(x1, y1, x2, y2);
	
	while (!pila.empty()) {
		putpixel(pila.top().getPCx(), pila.top().getPCy(), color);
		pila.pop();
	}

}


int checkNx;
int checkNx2;
int checkNy;
int checkNy2;
int posicionBalay;
int posicionBalax;

void dibujarNave(int speedY) {

	int inY = speedY * 5;
	int finY = speedY * 5;


	for (int i = 0; i <= 5; i++) {

		checkNx = 10;
		checkNx2 = 100;
		checkNy = 500 + i + inY;
		checkNy2 = 524 + i + inY;
		posicionBalay = 512 + i + inY;
		posicionBalax = 100;


		milinea(30,        checkNy       ,   90,  500 + i + finY, LIGHTGREEN);
		milinea(20,        506 + i + inY,    100, 506 + i + finY, GREEN);
		milinea(checkNx,   512 + i + inY,    20,  512 + i + finY, GREEN);
		milinea(50,        512 + i + inY,    70,  512 + i + finY, LIGHTGREEN);
		milinea(checkNx2,  512 + i + inY,    110, 512 + i + finY, RED);
		milinea(20,        518 + i + inY,    100, 518 + i + finY, GREEN);
		milinea(30,        checkNy2 ,        90,  524 + i + finY, LIGHTGREEN);

	}

}

void dibujarHongo(int speed) {
	int numHonX = numeroHongoX();
	int numHonY = numeroHongoY();

	
	int inX = speed * 10;
	int finX = speed * 10;
	for (int i = 0; i <= 5; i++) {

		milinea((numHonX)      + inX, numHonY      + i,  (numHonX + 20) + finX, numHonY      + i, BLUE);
		milinea((numHonX - 10) + inX, numHonY + 6  + i,  (numHonX + 30) + finX, numHonY + 6  + i, BLUE);
		milinea((numHonX - 20) + inX, numHonY + 12 + i,  (numHonX + 40) + finX, numHonY + 12 + i, BLUE);
		milinea((numHonX - 20) + inX, numHonY + 18 + i,  (numHonX + 40) + finX, numHonY + 18 + i, BLUE);
		milinea((numHonX - 20) + inX, numHonY + 24 + i,  (numHonX + 40) + finX, numHonY + 24 + i, BLUE);
		milinea((numHonX - 10) + inX, numHonY + 30 + i,  (numHonX + 30) + finX, numHonY + 30 + i, BROWN);
		milinea((numHonX - 10) + inX, numHonY + 36 + i,  (numHonX + 30) + finX, numHonY + 36 + i, BROWN);
		milinea((numHonX)      + inX, numHonY + 42 + i,  (numHonX + 20) + finX, numHonY + 42 + i, BROWN);
	}
}


void dibujarHelicoptero(int speed) {

	int inX = speed * 10;
	int finX = speed * 10;

	int numHeliY = numeroHelicY();
	int numHeliX = numeroAvX();

	for (int i = 0; i <= 5; i++) {
		milinea((numHeliX)      + inX, numHeliY +      i, (numHeliX + 70) + finX, numHeliY +      i, WHITE);
		milinea((numHeliX + 30) + inX, numHeliY + 6  + i, (numHeliX + 40) + finX, numHeliY + 6  + i, WHITE);
		milinea((numHeliX + 10) + inX, numHeliY + 12 + i, (numHeliX + 40) + finX, numHeliY + 12 + i, LIGHTMAGENTA);
		milinea((numHeliX + 70) + inX, numHeliY + 12 + i, (numHeliX + 80) + finX, numHeliY + 12 + i, LIGHTMAGENTA);
		milinea((numHeliX)      + inX, numHeliY + 18 + i, (numHeliX + 80) + finX, numHeliY + 18 + i, LIGHTMAGENTA);
		milinea((numHeliX)      + inX, numHeliY + 24 + i, (numHeliX + 60) + finX, numHeliY + 24 + i, LIGHTMAGENTA);
		milinea((numHeliX + 20) + inX, numHeliY + 30 + i, (numHeliX + 40) + finX, numHeliY + 30 + i, LIGHTMAGENTA);
		milinea((numHeliX + 10) + inX, numHeliY + 36 + i, (numHeliX + 60) + finX, numHeliY + 36 + i, WHITE);

	}
}

void dibujarAvioneta(int speed) {

	int numAVX = numeroAvX();
	int numAvY = numeroAvY();
	
	int inX = speed * 10;
	int finX = speed * 10;

	for (int i = 0; i <= 5; i++) {

		milinea((numAVX)      + inX, numAvY      + i,  (numAVX + 20) + finX, numAvY      + i, MAGENTA);
		milinea((numAVX + 30) + inX, numAvY      + i,  (numAVX + 50) + finX, numAvY      + i, MAGENTA);
		milinea((numAVX - 30) + inX, numAvY + 6  + i,  (numAVX - 20) + finX, numAvY + 6  + i, WHITE);
		milinea((numAVX - 10) + inX, numAvY + 6  + i,  (numAVX + 50) + finX, numAvY + 6  + i, MAGENTA);
		milinea((numAVX - 30) + inX, numAvY + 12 + i,  (numAVX - 20) + finX, numAvY + 12 + i, WHITE);
		milinea((numAVX - 20) + inX, numAvY + 12 + i,  (numAVX + 50) + finX, numAvY + 12 + i, MAGENTA);
		milinea((numAVX - 30) + inX, numAvY + 18 + i,  (numAVX - 20) + finX, numAvY + 18 + i, WHITE);
		milinea((numAVX - 10) + inX, numAvY + 18 + i,  (numAVX + 50) + finX, numAvY + 18 + i, MAGENTA);
		milinea((numAVX)      + inX, numAvY + 24 + i,  (numAVX + 30) + finX, numAvY + 24 + i, MAGENTA);
	}
}	

void moverNaveY() {

	if (GetAsyncKeyState(VK_UP)) {
		if (checkNy > 50) {
			movimientoY -= 5;
		}
	}
	else if (GetAsyncKeyState(VK_DOWN)) {
		if (checkNy < 500) {
			movimientoY += 5;
		}
	}
}

void moverHongo() {

	for (int i = 0; i < 1; i++) {
		movHongoY -= 5;
	}
}

void moverHelicoptero() {
	for (int i = 0; i < 1; i++) {
		movHelicY -= 5;
	}
}

void moverAvion() {
	for (int i = 0; i < 1; i++) {
		movAvioY -= 5;
	}
}

class Bala {	

	//milinea(posicionBalax + inX, posicionBalay + i, posicionBalax + 5 + finX, posicionBalay + i, YELLOW);

  public:
	  int posX,  posY;
	  int speed;

	  Bala() {
	  };

	  Bala(int speed) {
		 this->speed= speed;
	  }

	  int run(int x, int y, int color);

};

int Bala::run(int x, int y, int color)
{
	// increment the current speed
	speed += 50;

	// replace to x
	if ((x + speed) < getmaxx())
	{
		// bullet size in x position
		for (int i = 0; i <= 5; i++)
		{
			//milinea(x + i, y - speed, x + i, y - speed - 5, color);

			milinea(x + speed, y + i, x + speed + 5, y + i, color);
			//milinea(20, 40,80, 40, color);
			
		}
	}

	return x + speed + 5;
}

int inc = 0;

// munition
Bala arr[100];


void moverBala() {
	if (GetAsyncKeyState(VK_SPACE)) {

		inc ++;
	}
}






int main()
{
	for (int i = 0; i < 100; i++) {
		arr[i] = Bala(10);
	}


	initwindow(1200, 600, "Juego Graficaion" ,200,100);
	int pagina = 0;
	while (1) {
		if (pagina==0) {
			pagina = 1;
		}
		else {
			pagina = 0;
		}
		setactivepage(pagina);
		cleardevice();

		for (int i = 0; i < inc; i++) {
			int g = arr[i].run(posicionBalax , posicionBalay, YELLOW);
			std::cout << g << std::endl;
		}
		
		moverNaveY();
		dibujarNave(movimientoY);

		moverHongo();
		dibujarHongo(movHongoY);

		moverHelicoptero();
		dibujarHelicoptero(movHelicY);

		moverAvion();
		dibujarAvioneta(movAvioY);

		moverBala();
		
		setvisualpage(pagina);
		
		delay(1);
		
	}

	getch();
	closegraph();



	return 0;
}